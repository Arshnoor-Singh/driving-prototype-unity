using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

public class CarController_01 : MonoBehaviour
{
    public Rigidbody CarBody;

    [Header("Wheel Colliders")]
    public WheelCollider rearLeftColl;
    public WheelCollider rearRightColl;
    public WheelCollider frontLeftColl;
    public WheelCollider frontRightColl;

    [Header("Wheel Meshes")]
    public Transform rearLeftWheel;
    public Transform rearRightWheel;
    public Transform frontLeftWheel;
    public Transform frontRightWheel;

    [Header("Driving Variables")]
    public float horsePower = 500;
    public float maxSteerAngle = 25;
    public float brakeStrength = 2000;
    public float handBrakeStrength = 2000;
    public float minEngineRPM = 1000f;

    [Header("Gear Ratios")]
    public float gear_1_Ratio;
    public float gear_2_Ratio;
    public float gear_3_Ratio;
    public float gear_4_Ratio;
    public float gear_5_Ratio;
    public float gear_6_Ratio;
    public float gear_Reverse_Ratio;

    [Header("UI Public References")] [SerializeField]
    public Image rpmBarUI;
    public TextMeshProUGUI speedUI;
    public TextMeshProUGUI gearUI;

    [Header("Audio References")] 
    public AudioSource src;
 
    //Private Driving Controls Variables
    bool throttle;
    bool brake;
    bool handBrake;
    bool reverse;
    float currentRPM = 1000;
    float steeringDirection;
    float currentVelo;


    //--------------------------------------------------------------------------------------------------
    //INPUT FUNCTION 
    public void Acceleration(InputAction.CallbackContext context)
    {
        //This if statement checks if the player pressed the Up arrow Key
        if (context.started == true)
        {
            throttle = true;
            //If the key was just pressed, Set Throttle to True
        }

        //This statement checks if the player released the Up arrow Key
        if (context.canceled == true)
        {
            throttle = false;
            //If the key was just released, Set Throttle to False
        }
    }

    //INPUT FUNCTION
    public void Steering(InputAction.CallbackContext context)
    {
        steeringDirection = context.ReadValue<float>();
    }

    //INPUT FUNCTION
    public void Brakes(InputAction.CallbackContext context)
    {
        if(context.started == true)
        {
            brake = true;
        }

        if(context.canceled == true)
        {
            brake = false;
        }
    }

    //INPUT FUNCTION
    public void HandBrake(InputAction.CallbackContext context)
    {
        if (context.started == true)
        {
            handBrake = true;
        }

        if (context.canceled == true)
        {
            handBrake = false;
        }
    }

    //INPUT FUNCTION
    public void EngageReverse(InputAction.CallbackContext context)
    {
        AttemptReverse();
        Debug.LogWarning("Reverse Requested");
    }
    //---------------------------------------------------------------------------------------------------

    //Following Variables Are associated with the gears of the Car
    enum availableGears { gear_1, gear_2, gear_3, gear_4, gear_5, gear_6, gear_Reverse};

    //This struct defines what kind of values change with each gear
    struct gear
    {
        public availableGears currentGearName;
        public string gearNumber;
        public float maxGearRPM;
        public float minGearRPM;
        public float maxVelocity;
        public float minVelocity;
        public float gearRatio;
    }

    //Following are all the gear variable declarations
    gear firstGear;
    gear secondGear;
    gear thirdGear;
    gear fourthGear;
    gear fifthGear;
    gear sixthGear;
    gear reverseGear;
    gear currentGear;

    // Start is called before the first frame update
    void Start()
    {
        //Following is the data for the First Gear
        firstGear.currentGearName = availableGears.gear_1;
        firstGear.gearNumber = 1.ToString();
        firstGear.maxGearRPM = 4500f;
        firstGear.minGearRPM = 1000f;
        firstGear.maxVelocity = 20f;
        firstGear.minVelocity = 0.1f;
        firstGear.gearRatio = gear_1_Ratio;

        //Following is the data for the Second Gear
        secondGear.currentGearName = availableGears.gear_2;
        secondGear.gearNumber = 2.ToString();
        secondGear.maxGearRPM = 4900f;
        secondGear.minGearRPM = 2500f;
        secondGear.maxVelocity = 40f;
        secondGear.minVelocity = 21f;
        secondGear.gearRatio = gear_2_Ratio;

        //Following is the data for the Third Gear
        thirdGear.currentGearName = availableGears.gear_3;
        thirdGear.gearNumber = 3.ToString();
        thirdGear.maxGearRPM = 5500f;
        thirdGear.minGearRPM = 3000f;
        thirdGear.maxVelocity = 55f;
        thirdGear.minVelocity = 40f;
        thirdGear.gearRatio = gear_3_Ratio;

        //Following is the data for the Fourth Gear
        fourthGear.currentGearName = availableGears.gear_4;
        fourthGear.gearNumber = 4.ToString();
        fourthGear.maxGearRPM = 6000f;
        fourthGear.minGearRPM = 3000f;
        fourthGear.maxVelocity = 75f;
        fourthGear.minVelocity = 55f;
        fourthGear.gearRatio = gear_4_Ratio;

        //Following is the data for the Fifth Gear
        fifthGear.currentGearName = availableGears.gear_5;
        fifthGear.gearNumber = 5.ToString();
        fifthGear.maxGearRPM = 6000f;
        fifthGear.minGearRPM = 3300f;
        fifthGear.maxVelocity = 100f;
        fifthGear.minVelocity = 80f;
        fifthGear.gearRatio = gear_5_Ratio;

        //Following is the data for the Sixth Gear
        sixthGear.currentGearName = availableGears.gear_6;
        sixthGear.gearNumber = 6.ToString();
        sixthGear.maxGearRPM = 6600f;
        sixthGear.minGearRPM = 3500f;
        sixthGear.maxVelocity = 120f;
        sixthGear.minVelocity = 100;
        sixthGear.gearRatio = gear_6_Ratio;

        //Following is the data for the Reverse Gear
        reverseGear.currentGearName = availableGears.gear_Reverse;
        reverseGear.gearNumber = "R";
        reverseGear.maxGearRPM = 3000;
        reverseGear.minGearRPM = 1000f;
        reverseGear.maxVelocity = 25f;
        reverseGear.minVelocity = 0.1f;
        reverseGear.gearRatio = gear_Reverse_Ratio;

        currentGear = firstGear;

    }

    // Update is called once per frame
    void Update()
    {
        //Updates the RPM of the wheels
        UpdateRPM();
        

        //The following code Updates Wheel meshes from wheel colliders
        UpdateWheels(rearRightColl, rearRightWheel);
        UpdateWheels(rearLeftColl, rearLeftWheel);
        UpdateWheels(frontRightColl, frontRightWheel);
        UpdateWheels(frontLeftColl, frontLeftWheel);
        
        //Updates the UI 
        UpdateUI();

        Debug.LogWarning("Throttle = " + throttle + " |||| Current RPM = " + currentRPM + " |||| Brake = " + brake + " |||| Handbrake =" + handBrake + " |||| Steering Direction = " + steeringDirection + " |||| Currevt Velo = " + CarBody.velocity.magnitude * 2.36);
        Debug.Log("Current Gear = " + currentGear.currentGearName + " |||| Current Gear Maximum RPM = " + currentGear.maxGearRPM + " |||| Current Gear Max Velocity = " + currentGear.maxVelocity + " |||| Current Gear Min Velocity = " + currentGear.minVelocity);

    }

    //All physics based functions should be inside the Fixed Update.
    //This functions runs 50 times a second.
    private void FixedUpdate()
    {
        //Function calculates and applies torque
        AppliedTorque(currentRPM);

        //Funciton gets steering direction from Input Functions and Applies it to the front wheels
        ApplySteering(steeringDirection);

        //Apply Brakes
        ApplyBrakes(brake);

        //Apply HandBrake
        ApplyHandBrakes(handBrake);
    }

    //This function updates wheel meshes from wheel colliders
    void UpdateWheels(WheelCollider source, Transform target)
    {
        Vector3 tempPosition;
        Quaternion tempRotaion;

        source.GetWorldPose(out tempPosition, out tempRotaion);

        target.position = tempPosition;
        target.rotation = tempRotaion;
    }

    //This funciton updates the RPM each FRAME!!!!
    void UpdateRPM()
    {
        //The following line measures the Velocity from the RIGIDBODY component and converts it to Miles Per Hour
        currentVelo = CarBody.velocity.magnitude * 2.23f;
        
        //If you're pressing the accelerate button
        if (throttle)
        {
            //The following line increments the RPM based keeping into account the Horsepower
            currentRPM += horsePower/65;

            //If the current RPM has crossed the Max RPM of the current Gear, set it back to the Max RPM
            if(currentRPM >= currentGear.maxGearRPM)
            {
                currentRPM = currentGear.maxGearRPM;
            }

            //If the current RPM and Current Velocity have corssed the Max RPM and the Max Velo of the current Gear, Increment to the next Gear
            if(currentRPM >= currentGear.maxGearRPM && currentVelo > currentGear.maxVelocity)
            {
                //Change Gear to the next one.
                IncrementGear();
            }
        }

        //Decrement Current RPM if throttle is not pressed
        if(throttle == false)
        {
            currentRPM -= 30;
        }
        
        //If current RPM goes below Minimum Engine RPM, Set it back up to Min Engine RPM
        if(currentRPM <= minEngineRPM)
        {
            currentRPM = minEngineRPM;
        }

        if(currentVelo <= currentGear.minVelocity)
        {
            //Gear Decrement Requested
            DecrementGear();
            Debug.LogWarning("Gear Decrement Requested");
        }
    }

    //Function to calculate the amount of torque. It also applies torque using the wheel colliders
    void AppliedTorque(float currentRPM)
    {
        float AppliedTorque = ((horsePower * 5252) / currentRPM) / currentGear.gearRatio ;

        if (throttle == true)
        {
            rearLeftColl.motorTorque = -AppliedTorque;
            rearRightColl.motorTorque = -AppliedTorque;

        }

        if (throttle == false)
        {
            rearLeftColl.motorTorque = 0;
            rearRightColl.motorTorque = 0;
        }
    }

    //Function to apply steering
    void ApplySteering(float direction)
    {
        frontLeftColl.steerAngle = direction * maxSteerAngle;
        frontRightColl.steerAngle = direction * maxSteerAngle;
    }

    //Function to apply brake
    void ApplyBrakes(bool Brake)
    {
        if(Brake)
        {
            frontLeftColl.brakeTorque = brakeStrength;
            frontRightColl.brakeTorque = brakeStrength;
        }

        if(!Brake)
        {
            frontLeftColl.brakeTorque = 0;
            frontRightColl.brakeTorque = 0;
        }
    }

    //Function to apply Handbrake
    void ApplyHandBrakes(bool HandBrake)
    {
        if(HandBrake == true)
        {
            rearLeftColl.brakeTorque = handBrakeStrength;
            rearRightColl.brakeTorque = handBrakeStrength;
        }

        if(HandBrake == false)
        {
            rearLeftColl.brakeTorque = 0;
            rearRightColl.brakeTorque = 0;
        }
    }

    //Function to Incrementn Gears
    void IncrementGear()
    {
        switch(currentGear.currentGearName)
        {
            case availableGears.gear_1:
                currentGear = secondGear;
                currentRPM = secondGear.minGearRPM;
                Debug.LogWarning("Set gear to 2");
                break;

            case availableGears.gear_2:
                currentGear = thirdGear;
                currentRPM = thirdGear.minGearRPM;
                break;

            case availableGears.gear_3:
                currentGear = fourthGear;
                currentRPM = fourthGear.minGearRPM;
                break;

            case availableGears.gear_4:
                currentGear = fifthGear;
                currentRPM = fifthGear.minGearRPM;  
                break;

            case availableGears.gear_5:
                currentGear = sixthGear;
                currentRPM = sixthGear.minGearRPM;
                break;

            case availableGears.gear_6:
                Debug.LogWarning("Maximum Gear Reached. Cannot Increment");
                break;
        }
    }

    //Function to Decrement Gears
    void DecrementGear()
    {
        switch(currentGear.currentGearName)
        {
            case availableGears.gear_6:
                currentGear = fifthGear;
                break;

            case availableGears.gear_5:
                currentGear = fourthGear;
                break;

            case availableGears.gear_4:
                currentGear = thirdGear;
                break;

            case availableGears.gear_3:
                currentGear = secondGear;
                break;

            case availableGears.gear_2:
                currentGear = firstGear;
                break;

            case availableGears.gear_1:
                Debug.LogWarning("Cannot Decrement. Lowest Gear");
                break;
        }

    }

    //Following function attempts to put the car in Reverse Gear
    void AttemptReverse()
    {
        if(reverse == false && currentVelo <= 5)
        {
            reverse = true;
            currentGear = reverseGear;
            Debug.Log("Reverse Successful");
            return;
        }
        else if(reverse == true)
        {
            reverse = false;
            currentGear = firstGear;
            Debug.Log("Reverse DisEngaged. Now in First Gear");
            return;
        }
    }

    void UpdateUI()
    {
        rpmBarUI.fillAmount = currentRPM / currentGear.maxGearRPM;
        speedUI.text = Mathf.FloorToInt(currentVelo * 1.2f).ToString();
        gearUI.text = currentGear.gearNumber;
    }
}
